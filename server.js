const express = require("express")
const bodyParser = require('body-parser')

const session = require('express-session')
require('dotenv').config()

const mongoose = require('mongoose')

const app = express()
const port = process.env.APP_PORT || 5000

const auth = require('./controllers/AuthController')

const apiRoutes = require('./api-routes')


app.use(session({
    secret: "abcdefg",
    saveUninitialized: false,
    resave: true,
    cookie: {
        maxAge: 1000 * 60 * 60
    }
}))


app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))
app.set('view engine', 'ejs')
app.use('/', express.static('public'))
// app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css'))

app.get('/', auth.UserLogin)

app.use('/api', apiRoutes)

mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true }, () => console.log('Connected to Mongo'))

app.listen(port, () => {
    console.log("Listening to port ", port)
})