const response = require('../response')
const axios = require('axios')
const Activity = require('../models/Activity')
const Account = require('../models/Account')

const syncActivities = async (req, res) => {


    try {
        const allActivities = await axios.get(process.env.STRAVA_API_URL + '/athlete/activities', {
            headers: {
                Authorization: "Bearer " + req.body.tokens
            },

            params: {
                before: Date.now() - 1000 * 3600 * 24 * 3,
                page: 1,
                per_page: 10
            }
        })

        let all = allActivities.data

        

        for (let activity in all) {


            let savedActivity = await Activity.findOneAndUpdate({ 'id': all[activity].id}, all[activity])

            if (!savedActivity) {
                let saving = new Activity(all[activity])
                saving.save().then(success => response.ok(success, res)).catch(err => response.failure(err, res))
            }


        }
        return res.redirect('/?synced=1')

    } catch (error) {
        return response.failure(error.message, res)
    }
}

const syncActivityPostman = async (req, res) => {
    try {
        const allActivities = await axios.get(process.env.STRAVA_API_URL + '/athlete/activities', {
            headers: {
                Authorization: "Bearer " + req.body.tokens
            },

            params: {
                before: Date.now() - 1000 * 3600 * 24 * 3,
                page: 1,
                per_page: 10
            }
        })

        let all = allActivities.data

        

        for (let activity in all) {


            let savedActivity = await Activity.findOneAndUpdate({ 'id': all[activity].id}, all[activity])

            if (!savedActivity) {
                let saving = new Activity(all[activity])
                saving.save().then(success => response.ok(success, res)).catch(err => response.failure(err, res))
            }


        }
        return response.ok(all, res)

    } catch (error) {
        return response.failure(error.message, res)
    }
}

const showActivities = async (req, res) => {

    let criteria = {}

    if (typeof req.query.type != "undefined") {

        let typeQuery = capitalizeFirstLetter(req.query.type.toLowerCase())

        criteria['type'] = typeQuery
    }

    if (typeof req.headers.token != "undefined") {
        let user = await Account.findOne({'access_token': req.headers.token})
        if (!user) {
            return response.notFound("User", res)
        }

        criteria["athlete.id"] = user.athlete.id
    }

    let myActivities = await Activity.find(criteria)

    if (myActivities.length < 1) {
        response.notFound("Activity", res)
    }

    response.ok(myActivities, res)
}

const getActivity = async (req, res) => {
    let user = await Account.findOne({'access_token': req.headers.token})

    if (!user) {
        return response.forbidden(res)
    }


    let activity = await Activity.findOne({ 'athlete.id': user.athlete.id, id: req.params.activity_id })

    if (!activity) {
        return response.notFound("Activity", res)
    }

    return response.ok(activity, res)

}

const deleteActivity = async (req, res) => {
    let user = await Account.findOne({'access_token': req.headers.token})

    if (!user) {
        return response.forbidden(res)
    }

    let activity = await Activity.findOneAndRemove({ 'athlete.id': user.athlete.id, id: req.params.activity_id})

    if (!activity) {
        return response.notFound("Activity", res)
    }

    return response.customResponse("Activity was deleted successfully", true, 200, res)

}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

module.exports = {
    syncActivities, showActivities, getActivity, deleteActivity, syncActivityPostman
}