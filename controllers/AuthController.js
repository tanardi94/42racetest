const response = require('../response')
const axios = require('axios')
const express = require('express')
const app = express()
const Account = require('../models/Account')

const session = require('express-session')
const Activity = require('../models/Activity')


app.set('view engine', 'ejs')


const UserLogin = async (req, res) => {

    // req.session.destroy();

    if (typeof req.session.user != "undefined") {

        if (req.query.synced && req.query.synced == 1) {
            let all = []
            try {

                let activities = await Activity.find({ 'athlete.id': req.session.user.athlete.id })


                for (let i in activities) {
                    all.push(activities[i])
                }

            } catch (error) {
                let all = []
            }
            req.session.activities = all
        } 

        res.render('index', {
            data: req.session.user,
            activities: req.session.activities
        })
    } else {

        if (typeof req.query.client != "undefined") {
            res.render('index', {
                client: req.query.client
            })
        }
        
        try {
            
            const fetch = await axios.post(process.env.STRAVA_URL + '/oauth/token', {
                client_id: process.env.STRAVA_CLIENT_ID,
                client_secret: process.env.STRAVA_CLIENT_SECRET,
                code: req.query.code,
                grant_type: "authorization_code"
            })
    
            const user = await Account.findOneAndUpdate({'athlete.id': fetch.data.athlete.id}, {
                access_token: fetch.data.access_token
            })
    
            if (!user) {
                const Athletes = new Account(fetch.data)
    
                Athletes.save().then(data => {
                    req.session.user = data
                    res.render('index', {
                        data: req.session.user,
                    })
    
                    
                }).catch(err => {
                    res.render('index')
                })
            } else {
                req.session.user = user
                res.render('index', {
                    data: req.session.user
                })
            }
    
            
    
        } catch (error) {
            res.render('index')
        }
    }
}


const authorizeApp = async (req, res) => {
    try {
            
        const fetch = await axios.post(process.env.STRAVA_URL + '/oauth/token', {
            client_id: process.env.STRAVA_CLIENT_ID,
            client_secret: process.env.STRAVA_CLIENT_SECRET,
            code: req.body.tokens,
            grant_type: "authorization_code"
        })

        const user = await Account.findOneAndUpdate({'athlete.id': fetch.data.athlete.id}, {
            access_token: fetch.data.access_token
        })

        if (!user) {
            const Athletes = new Account(fetch.data)

            Athletes.save().then(data => {
                response.ok(Athletes, res)

                
            }).catch(err => {
                response.failure(err, res)
            })
        } else {
            response.ok(user, res)
        }

        

    } catch (error) {
        response.failure(error.response, res)
    }
}


const deauthorizeApp = async (req, res) => {
    
    let account = await Account.findOne({ 'access_token': req.body.logout_token })
    if (!account) {
        return response.forbidden(res)
    }


    try {
        let deauthorize = await axios.post(process.env.STRAVA_URL + '/oauth/deauthorize', {
            headers: {
                Authorization: "Bearer " + req.body.logout_token
            },
        })
        
        req.session.destroy()
        return response.ok(deauthorize.data, res)
        
    } catch (error) {
        return response.failure(error.message, res)
    }

}

module.exports = {
    UserLogin, deauthorizeApp, authorizeApp
}