const response = require('../response')
const axios = require('axios')
const Account = require('../models/Account')


const accountLists = async (req, res) => {
    try {
        
        let accounts = await Account.find()

        if (accounts.length < 0) {
            response.notFound("Account", res)
        }

        response.ok(accounts, res)

    } catch (error) {
        response.failure(error.response, res)
    }
}



const getUser = async (req, res) => {
    try {
        
        let account = await Account.findOne({"athlete.id": req.query.athlete_id})

        if (!athlete) {
            response.notFound("Account", res)
        }

        response.ok(athlete, res)

    } catch (error) {
        response.failure(error, res)
        
    }
}

module.exports = {
    accountLists, getUser
}