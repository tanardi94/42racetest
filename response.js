'use strict'


exports.ok = function(values, res) {
    var data = {
        success: true,
        data: values
    };
    res.json(data).status(200);
    res.end();
};

exports.customResponse = (message, status, code, res) => {
    var data = {
        success: status,
        message: message
    };
    res.json(data).status(code);
    res.end();
}

exports.failure = function(message, res) {
    var data = {
        success: false,
        message: message
    };
    res.json(data).status(400);
    res.end();
};

exports.notFound = function(message, res) {
    var data = {
        success: false,
        message: message + " is not found"
    };
    res.json(data).status(404);
    res.end();
};

exports.created = function(message, res) {
    var data = {
        success: true,
        message: message + " is successfully created"
    };
    res.json(data).status(201);
    res.end();
}

exports.invalidParameter = function(message, res) {
    var data = {
        success: false,
        message: message
    };
    res.json(data).status(422);
    res.end();
}

exports.forbidden = function(res) {
    var data = {
        success: false,
        message: "You have no access for this service"
    };

    res.json(data).status(403);
    res.end();
}

exports.unauthenticated = function(res) {
    var data = {
        success: false,
        message: "You are unauthenticated"
    };

    res.json(data).status(401);
    res.end();
}