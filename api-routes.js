const express = require('express')

const router = express.Router()

const Account = require('./controllers/AccountController')
const Auth = require('./controllers/AuthController')
const activity = require('./controllers/ActivityController')



router.route('/').get(async (req, res) => {
    res.json({
        success: true,
        message: "This is api routing"
    })
})

router.route('/account/:id').get(Account.getUser)
router.route('accounts').get(Account.accountLists)
router.route('/login').post(Auth.authorizeApp)
router.route('/logout').post(Auth.deauthorizeApp)

router.route('/activities/sync').post(activity.syncActivityPostman)
router.route('/activities/syncing').post(activity.syncActivities)
router.route('/activities').get(activity.showActivities)
router.route('/activity/:activity_id').get(activity.getActivity).delete(activity.deleteActivity)



module.exports = router