const mongoose = require('mongoose')



const ActivitySchema = mongoose.Schema({
    athlete: Object,
    resource_state: {
        type: Number
    },
    athlete: {
        id: {
        type: Number
        },
        resource_state: {
        type: Number
        }
    },
    name: {
        type: String
    },
    distance: {
        type: Number
    },
    moving_time: {
        type: Number
    },
    elapsed_time: {
        type: Number
    },
    total_elevation_gain: {
        type: Number
    },
    type: {
        type: String
    },
    workout_type: {
        type: Number,
        required: false
    },
    id: {
        type: Number
    },
    external_id: {
        type: String
    },
    upload_id: {
        type: Number
    },
    start_date: {
        type: Date
    },
    start_date_local: {
        type: Date
    },
    timezone: {
        type: String
    },
    utc_offset: {
        type: Number
    },
    start_latlng: {
        type: Array,
        required: false
    },
    end_latlng: {
        type: Array,
        required: false
    },
    location_city: {
        type: Number,
        required: false
    },
    location_state: {
        type: Number,
        required: false
    },
    location_country: {
        type: String
    },
    achievement_count: {
        type: Number
    },
    kudos_count: {
        type: Number
    },
    comment_count: {
        type: Number
    },
    athlete_count: {
        type: Number
    },
    photo_count: {
        type: Number
    },
    map: {
        id: {
        type: String
        },
        summary_polyline: {
            type: String,
            required: false
        },
        resource_state: {
        type: Number
        }
    },
    trainer: {
        type: Boolean
    },
    commute: {
        type: Boolean
    },
    manual: {
        type: Boolean
    },
    private: {
        type: Boolean
    },
    flagged: {
        type: Boolean
    },
    gear_id: {
        type: String
    },
    from_accepted_tag: {
        type: Boolean
    },
    average_speed: {
        type: Number
    },
    max_speed: {
        type: Number
    },
    average_cadence: {
        type: Number
    },
    average_watts: {
        type: Number
    },
    weighted_average_watts: {
        type: Number
    },
    kilojoules: {
        type: Number
    },
    device_watts: {
        type: Boolean
    },
    has_heartrate: {
        type: Boolean
    },
    average_heartrate: {
        type: Number
    },
    max_heartrate: {
        type: Number
    },
    max_watts: {
        type: Number
    },
    pr_count: {
        type: Number
    },
    total_photo_count: {
        type: Number
    },
    has_kudoed: {
        type: Boolean
    },
    suffer_score: {
        type: Number
    }
})


module.exports = mongoose.model('Activity', ActivitySchema)