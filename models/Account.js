const mongoose = require('mongoose')

const AccountSchema = mongoose.Schema({
    // name: {
    //     type: String,
    //     required: true
    // },

    access_token: String,
    token_expires: Number,
    athlete: {
        id: Number,
        firstname: String,
        lastname: String,
        username: String,
        sex: String,
    },
    created_at: {
        type: Date,
        default: Date.now()
    },
    
    updated_at: {
        type: Date,
        default: Date.now()
    }
    

    
})


module.exports = mongoose.model('Account', AccountSchema)